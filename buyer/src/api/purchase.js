import request, { Method } from '@/utils/request'
import { api } from '~/ui-domain'

/**
 * 发布采购单
 * @param params
 * @returns {Promise | Promise<unknown>}
 */
export function addPurchase(params) {
  return request({
    url: `${api.b2b}/b2b/purchase/orders`,
    method: Method.POST,
    needToken: true,
    data: params
  })
}

/**
 * 获取采购单列表
 * @param params
 * @returns {AxiosPromise | Promise | Promise<unknown>}
 */
export function getPurchases(params) {
  return request({
    url: `${api.b2b}/b2b/purchase/orders`,
    method: Method.GET,
    needToken: true,
    params
  })
}

/**
 * 关闭采购单
 * @param id
 * @returns {AxiosPromise | Promise | Promise<unknown>}
 */
export function closePurchase(id) {
  return request({
    url: `${api.b2b}/b2b/purchase/orders/close/${id}`,
    method: Method.PUT,
    needToken: true
  })
}

/**
 * 删除采购单
 * @param id
 * @returns {AxiosPromise | Promise | Promise<unknown>}
 */
export function deletePurchase(id) {
  return request({
    url: `${api.b2b}/b2b/purchase/orders/${id}`,
    method: Method.DELETE,
    needToken: true
  })
}

/**
 * 获取采购单详情
 * @param id
 * @returns {AxiosPromise | Promise | Promise<unknown>}
 */
export function getPurchaseDetail(id) {
  return request({
    url: `${api.b2b}/b2b/purchase/orders/${id}`,
    method: Method.GET,
    // TODO 查看详情需要登录
    needToken: true
  })
}

/**
 * 通过搜索获取采购单列表
 * @param params
 * @returns {AxiosPromise | Promise | Promise<unknown>}
 */
export function getPurchaseListBySearch(params) {
  return request({
    url: `${api.b2b}/b2b/purchase/orders/search`,
    method: Method.GET,
    needToken: true,
    params
  })
}

/**
 * 获取采购单下的报价单列表
 * @param params
 * @returns {AxiosPromise | Promise | Promise<unknown>}
 */
export function getQuotationsByPurchaseId(params) {
  return request({
    url: `${api.b2b}/b2b/purchase/quotations`,
    method: Method.GET,
    needToken: true,
    params
  })
}

/**
 * 关闭采购单下的报价单
 * @param id
 * @returns {AxiosPromise | Promise | Promise<unknown>}
 */
export function closeQuotation(id) {
  return request({
    url: `${api.b2b}/b2b/purchase/quotations/close/${id}`,
    method: Method.PUT,
    needToken: true
  })
}

/**
 * 获取报价单详情
 * @param id
 * @returns {AxiosPromise | Promise | Promise<unknown>}
 */
export function getQuotationDetail(id) {
  return request({
    url: `${api.b2b}/b2b/purchase/quotations/${id}`,
    method: Method.GET,
    needToken: true
  })
}
