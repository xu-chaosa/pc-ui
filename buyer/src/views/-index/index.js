/**
 * Created by Andste on 2018/6/6.
 */

import IndexCard from './-card'
import IndexSearchBar from './-search-bar'
import IndexSeckill from './-seckill'
import IndexSkeleton from './-index-skeleton'

export default {
  IndexCard,
  IndexSearchBar,
  IndexSeckill,
  IndexSkeleton
}
